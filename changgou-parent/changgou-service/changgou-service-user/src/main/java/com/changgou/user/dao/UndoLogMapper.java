package com.changgou.user.dao;
import com.changgou.user.pojo.UndoLog;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:UndoLog的Dao
 *****/
public interface UndoLogMapper extends Mapper<UndoLog> {
}
