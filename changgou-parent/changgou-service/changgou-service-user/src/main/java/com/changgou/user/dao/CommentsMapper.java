package com.changgou.user.dao;
import com.changgou.user.pojo.Comments;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Comments的Dao
 *****/
@Repository
public interface CommentsMapper extends Mapper<Comments> {
}
