package com.changgou.user.dao;
import com.changgou.user.pojo.Cities;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Cities的Dao
 *****/
@Repository
public interface CitiesMapper extends Mapper<Cities> {
}
