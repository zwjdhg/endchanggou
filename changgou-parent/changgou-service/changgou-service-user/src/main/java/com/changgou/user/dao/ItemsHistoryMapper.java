package com.changgou.user.dao;
import com.changgou.user.pojo.ItemsHistory;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:ItemsHistory的Dao
 *****/
public interface ItemsHistoryMapper extends Mapper<ItemsHistory> {
}
