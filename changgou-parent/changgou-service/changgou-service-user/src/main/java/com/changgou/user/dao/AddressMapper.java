package com.changgou.user.dao;
import com.changgou.user.pojo.Address;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/****
 * @Author:deng
 * @Description:Address的Dao
 *****/
@Repository
public interface AddressMapper extends Mapper<Address> {
    @Select("SELECT * FROM `tb_address` WHERE username=#{username}")
    List<Address> findAllByName(String username);
}
