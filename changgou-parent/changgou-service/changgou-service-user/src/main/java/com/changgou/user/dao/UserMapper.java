package com.changgou.user.dao;
import com.changgou.user.pojo.User;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:User的Dao
 *****/
public interface UserMapper extends Mapper<User> {
    @Update("update tb_user set points = points + #{points} where username=#{username} ")
    void addPoints(String username, Integer points);
}
