package com.changgou;

import io.jsonwebtoken.*;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: JwtTest
 * @Author: ZWJ
 * @Date: 2020-09-02 19:42
 * @Synopsis: Null
 **/
public class JwtTest {
    /****
     * 创建Jwt令牌
     */
    @Test
    public void jwt01(){
        JwtBuilder builder = Jwts.builder()
                .setId("111")  //设置唯一编号
                .setSubject("霓虹") //设置主题 可以是json数据
                .setIssuedAt(new Date()) //设置签发日期
                //.setExpiration(new Date(System.currentTimeMillis()+60000)) //设置过期时间
                .signWith(SignatureAlgorithm.HS256,"java96"); //设置签名，使用HS256算法，并设置SecretKey(字符串)
       //构建，并返回一个字符串
        String compact = builder.compact();
        //eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMTEiLCJzdWIiOiLpnJPombkiLCJpYXQiOjE1OTkwNDcyNzJ9.GT9qNgGCzOpFd-vp5-r8CrhC3ONYe7tl93G7AQGWrag
        //eyJhbGciOiJIUzI1NiJ9.
        // eyJqdGkiOiIxMTEiLCJzdWIiOiLpnJPombkiLCJpYXQiOjE1OTkwNDcyNzJ9.
        // GT9qNgGCzOpFd-vp5-r8CrhC3ONYe7tl93G7AQGWrag
        System.out.println(compact);
    }
    /***
     * 解析Jwt令牌数据
     */
    @Test
    public void testParseJwt(){
        //Jwt令牌数据
        String compactJwt="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxMTEiLCJzdWIiOiLpnJPombkiLCJpYXQiOjE1OTkwNTExNjV9.kpnJ3CPcn31HhYACYjqHx4DqbaN3ZlgeSpM86snmh8I";
        Claims java96 = Jwts.parser()
                .setSigningKey("java96")
                .parseClaimsJws(compactJwt)
                .getBody();
        System.out.println(java96);
    }
    /****
     * 自定义数据创建Jwt令牌
     */
    @Test
    public void testCreateJwt(){
        JwtBuilder builder = Jwts.builder()
                .setId("888")  //设置唯一编号
                .setIssuedAt(new Date()) //设置签发日期
                .signWith(SignatureAlgorithm.HS256,"java96"); //设置签名，设置算法，并设置key
        //自定义数据
        Map<String,Object> userInfo=new HashMap<>();
        userInfo.put("name","王五");
        userInfo.put("age",28);
        userInfo.put("address","大街上");
        //将自定义数据放入
        builder.addClaims(userInfo);
        //构建并返回一个字符串字符串
        String compact = builder.compact();
        System.out.println(compact);
    }
    @Test
public void testParseJwt01(){
        String ss="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJpYXQiOjE1OTkwNTA3NTMsImFkZHJlc3MiOiLlpKfooZfkuIoiLCJuYW1lIjoi546L5LqUIiwiYWdlIjoyOH0.tHobLIa9x0wxl8H-CcYfofemXwd3wolwTniYpu4F1jo";
        Claims java96 = Jwts.parser()
                .setSigningKey("java96")
                .parseClaimsJws(ss)
                .getBody(); //不加getBody怎会显示密钥加密后的字符串
        System.out.println(java96);
    }
}
