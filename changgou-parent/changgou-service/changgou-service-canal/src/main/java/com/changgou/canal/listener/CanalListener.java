package com.changgou.canal.listener;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.otter.canal.protocol.CanalEntry;

import com.changgou.content.Feign.ContentFeign;
import com.changgou.content.pojo.Content;
import com.changgou.util.Result;
import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.DeleteListenPoint;
import com.xpand.starter.canal.annotation.InsertListenPoint;
import com.xpand.starter.canal.annotation.UpdateListenPoint;
import com.xpand.starter.canal.client.CanalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @ClassName: CanalListener
 * @Author: ZWJ
 * @Date: 2020-08-28 16:29
 * @Synopsis: Null
 **/
@CanalEventListener
public class CanalListener {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ContentFeign contentFeign;
    /*
    * @Description: 增加监听服务
    * @param eventType
    * @param rowData
    * @return: 14265
    */
    @InsertListenPoint
    public void insertListener(CanalEntry.EventType eventType,CanalEntry.RowData rowData){
        //新增之后的数据
        List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();
        for (CanalEntry.Column column : afterColumnsList) {
            String name = column.getName();//列名
            String value = column.getValue();//该列的值
            if (name.equals("category_id")){
                //查询数据库category_id=value的数据
                Result<List<Content>> categoryId = contentFeign.findCategoryId(Long.valueOf(value));
                //将redis中category_id=value的数据删除掉
                //将查询到的数据添加到redis中
                stringRedisTemplate.boundValueOps("content_"+ value)
                        .set(JSONObject.toJSONString(categoryId.getData()));
            }
        }
    }
    //当数据库进行修改后
    @UpdateListenPoint
    public void updateListener(CanalEntry.RowData rowData){
        System.out.println("他进行修改了了");
        //修改前的数据
        List<CanalEntry.Column> beforeColumnsList = rowData.getBeforeColumnsList();
        for (CanalEntry.Column column : beforeColumnsList) {
            //列名
            String name = column.getName();
            //该列的值
            String value = column.getValue();
            if (name.equals("category_id")){
                //获取修改后该分类id下的数据
                Result<List<Content>> categoryId = contentFeign.findCategoryId(Long.valueOf(value));
                List<Content> data = categoryId.getData();
                //判断数据库是否有该分类id的数据
                if (data!=null){
                    //有：将redis中的数据进行覆盖
                    stringRedisTemplate.boundValueOps("content_"+value).set(JSONObject.toJSONString(data));
                }else {
                    //无则将redis中数据删除
                    stringRedisTemplate.delete("content_"+value);
                }

            }
        }
        //修改之后的数据
        List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();
        for (CanalEntry.Column column : afterColumnsList) {
            //列名
            String name = column.getName();
            //该列的值
            String value = column.getValue();
            if (name.equals("category_id")){
                //获取修改后的该分类id的数据
                Result<List<Content>> categoryId = contentFeign.findCategoryId(Long.valueOf(value));
                List<Content> data = categoryId.getData();
                stringRedisTemplate.boundValueOps("content_"+value).set(JSONObject.toJSONString(data));
            }
        }
    }
    //当数据库进行删除时
    @DeleteListenPoint
    public void deleteListener(CanalEntry.RowData rowData){
        System.out.println("他进行删除了");
        //获取修改前的代码
        List<CanalEntry.Column> beforeColumnsList = rowData.getBeforeColumnsList();
        for (CanalEntry.Column column : beforeColumnsList) {
            //获取列名
            String name = column.getName();
            //获取该列名的值
            String value = column.getValue();
            if (name.equals("category_id")){
                //获取category_id数据
                Result<List<Content>> categoryId = contentFeign.findCategoryId(Long.valueOf(value));
                List<Content> data = categoryId.getData();
                if (data==null){
                    //数据库中无数据则将redis中数据删除
                    stringRedisTemplate.delete("content_"+value);
                }{
                    //数据库中还有数据则将redis数据进行覆盖
                    stringRedisTemplate.boundValueOps("content_"+value).set(JSONObject.toJSONString(data));
                }
            }
        }
    }
}
