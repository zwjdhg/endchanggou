package com.changgou.controllor;

import com.changgou.file.FastDFSFile;
import com.changgou.util.FastDfsClient;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @ClassName: FileController
 * @Author: ZWJ
 * @Date: 2020-08-24 20:29
 * @Synopsis: Null
 **/
@RestController
@CrossOrigin
public class FileController {

    @PostMapping("/upload")
    public String upload(@RequestParam("file")MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        FastDFSFile fastDfsFile = new FastDFSFile();
        fastDfsFile.setName(originalFilename);
        fastDfsFile.setContent(bytes);
        fastDfsFile.setExt(StringUtils.getFilenameExtension(file.getOriginalFilename()));
        String[] upload = FastDfsClient.upload(fastDfsFile);
        return "http://192.168.211.132:8080"+"/"+upload[0]+"/"+upload[1];
    }

}
