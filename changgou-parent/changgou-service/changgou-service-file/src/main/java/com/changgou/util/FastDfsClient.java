package com.changgou.util;

import com.changgou.file.FastDFSFile;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import feign.Client;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;

import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoProperties;
import org.springframework.core.io.ClassPathResource;

import java.io.*;

/**
 * @ClassName: FastDfsClient
 * @Author: ZWJ
 * @Date: 2020-08-25 14:39
 * @Synopsis: Null
 **/
public class FastDfsClient {
    //初始化tracker信息
    static {
        try {
            //获取tracker的配置文件的位置
            String filePath = new ClassPathResource("fdfs_client.conf").getPath();
            //加载tracker配置
            ClientGlobal.init(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * @Description: 文件上传
     * @Param: file: 要上传的文件信息封装->FastDFSFile
     * @create: 2020/8/25 15:12
     * @return: String[]
     *           1：文件上传所存储的组名
     *           2：文件存储路劲
     */
    public static String[] upload(FastDFSFile file) {
        //获取文件作者
        NameValuePair[] nameValuePairs = new NameValuePair[1];
        nameValuePairs[0] = new NameValuePair(file.getAuthor());
        try {
           /* //创建TrackerClient客户端对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient对象获取TrackerServer信息
            TrackerServer trackerServer = trackerClient.getConnection();*/

        /*   //通过静态方法获取TrackerServer对象
            TrackerServer trackerServer = getTrackerServer();
            //创建StorageClient对象
            StorageClient storageClient = new StorageClient(trackerServer, null);*/

            //通过静态方法获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //执行文件上传
            //文件上传后的返回值
            //uploadResults[0]:文件上传所存储的组名
            //uploadResults[1]:文件存储路径
            String[] uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), null);
            return uploadResults;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * @Description: 获取文件信息
     * @Param: groupName：组名
     * @Param: remoteFileName：文件存储完整名
     * @create: 2020/8/25 15:29
     * @return: FileInfo
     */
    public static FileInfo getFile(String groupName, String remoteFileName) {
        try {
            /*//创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //用TrackerClient对象创建TrackerServer
            TrackerServer trackerServer = trackerClient.getConnection();*/

           /* //通过静态方法获取TrackerServer
            TrackerServer trackerServer = getTrackerServer();
            //创建StorageClient对象
            StorageClient storageClient = new StorageClient(trackerServer, null);*/

            //通过静态方法获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //获取文件信息
            FileInfo file_info = storageClient.get_file_info(groupName, remoteFileName);
            return file_info;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * @Description: 文件下载
     * @Param: groupName：组名
     * @Param: remoteFileName：文件存储完整名
     * @create: 2020/8/25 15:46
     * @return:
     */
    public static InputStream downFile(String groupName, String remoteFileName) {
        try {
           /* //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //用TrackerClient对象创建TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();*/

         /*  //通过静态方法获取TrackerServer
            TrackerServer trackerServer = getTrackerServer();
            //创建StorageClient对象
            StorageClient storageClient = new StorageClient(trackerServer, null);*/

            //通过静态方法获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //通过StorageClient对象下载文件
            byte[] bytes = storageClient.download_file(groupName, remoteFileName);
            //将字节组对象转换成字节输入流
            return new ByteArrayInputStream(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * @Description: 文件删除
     * @Param: groupName：组名
     * @Param: remoteFileName：文件存储完整名
     * @create: 2020/8/25 15:58
     * @return:
     */
    public static Integer deleteFile(String groupName, String remoteFileName) {
        try {
         /*   //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient对象获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();*/

        /* //通过静态方法获取TrackerServer对象
            TrackerServer trackerServer = getTrackerServer();
            //创建StorageClient对象
            StorageClient storageClient = new StorageClient(trackerServer, null);*/
        //通过静态方法获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //通过StorageClient删除文件
            int i = storageClient.delete_file(groupName, remoteFileName);
            return i;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获取组信息
     * @param groupName :组名
     */
    public static StorageServer getStorages(String groupName) {
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //通过trackerClient获取Storage组信息
            return trackerClient.getStoreStorage(trackerServer, groupName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 根据文件组名和文件存储路径获取Storage服务的IP、端口信息
     * @param groupName :组名
     * @param remoteFileName ：文件存储完整名
     */
    public static ServerInfo[] getServerInfo(String groupName, String remoteFileName) {
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //获取服务信息
            return trackerClient.getFetchStorages(trackerServer, groupName, remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获取Tracker服务地址
     */
    public static String getTrackerUrl() {
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //获取Tracker地址
            return "http://" + trackerServer.getInetSocketAddress().getHostString() + ":" + ClientGlobal.getG_tracker_http_port();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * @Description: 进行优化进行提取公共代码
     * @Param:
     * @create: 2020/8/25 16:08
     * @return: TrackerServer
     */
    public static TrackerServer getTrackerServer() {
        try {
            TrackerClient trackerClient = new TrackerClient();
            TrackerServer trackerServer = trackerClient.getConnection();
            return trackerServer;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    /*
    * @Description:  进行优化进行提取公共代码
    * @Param: []
    * @create: 2020/8/25 16:21
    * @return: StorageClient
    */
    public static StorageClient getStorageClient(){
        TrackerServer trackerServer = getTrackerServer();
        StorageClient storageClient=new StorageClient(trackerServer,null);
        return storageClient;
    }


    public static void main(String[] args) throws Exception {
        /*//文件下载
        InputStream inputStream = downFile("group1", "M00/00/00/wKjThF9EzVuASAaQAADd11dZDg0854.png");
        OutputStream outputStream=new FileOutputStream("D://美图//a.png");
        byte[] bytes=new byte[1024];
        while (inputStream.read(bytes)!=-1){
            outputStream.write(bytes);
        }*/

      /*  //获取文件信息
        FileInfo group1 = getFile("group1", "M00/00/00/wKjThF9EzVuASAaQAADd11dZDg0854.png");
        System.out.println(group1);*/
      //删除文件
        Integer group1 = deleteFile("group1", "M00/00/00/wKjThF9EzVuASAaQAADd11dZDg0854.png");
        System.out.println(group1);

    }
}
