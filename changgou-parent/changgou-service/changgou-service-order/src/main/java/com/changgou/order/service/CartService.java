package com.changgou.order.service;

import com.changgou.order.pojo.OrderItem;
import com.changgou.util.Result;

import java.util.List;

/**
 * @ClassName: CartService
 * @Author: ZWJ
 * @Date: 2020-09-06 15:44
 * @Synopsis: Null
 **/
public interface CartService {

    /***
     * 添加购物车
     * @param num:购买商品数量
     * @param id：购买ID
     * @param username：购买用户
     * @return
     */
    void add(Integer num, String id, String username);
    /*
     * 查询购物车
     * @Param: username
     * @create: 2020/9/6 16:58
     * @return: 14265
     */
    List<OrderItem> findAll(String username);

    List<OrderItem> getChoose(String username,String[] ids);
}
