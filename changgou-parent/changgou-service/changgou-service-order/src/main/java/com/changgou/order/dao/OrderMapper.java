package com.changgou.order.dao;
import com.changgou.order.pojo.Order;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Order的Dao
 *****/
public interface OrderMapper extends Mapper<Order> {
	List<Order> findByTime(@Param("month") Integer month,@Param("username") String username);

}
