package com.changgou.order.dao;
import com.changgou.order.pojo.OrderConfig;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:OrderConfig的Dao
 *****/
public interface OrderConfigMapper extends Mapper<OrderConfig> {
}
