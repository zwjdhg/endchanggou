package com.changgou.order.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.changgou.goods.Feign.SpuFeign;
import com.changgou.goods.Feign.skuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.goods.pojo.Spu;
import com.changgou.order.pojo.OrderItem;
import com.changgou.order.service.CartService;
import com.changgou.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: CartServiceImpl
 * @Author: ZWJ
 * @Date: 2020-09-06 15:45
 * @Synopsis: Null
 **/
@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private skuFeign skuFeign;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private SpuFeign spuFeign;

    /***
     * 添加购物车
     * @param num :购买商品数量
     * @param id ：购买ID
     * @param username ：购买用户
     * @return
     */
    @Override
    public void add(Integer num, String id, String username) {
        if (num<=0){
            redisTemplate.boundHashOps("Cart_" + username).delete(id);
            return;
        }
        //查询商品
        Result<Sku> skuResult = skuFeign.findById(id);
        Sku sku = skuResult.getData();
        if (sku!=null){
                //根据商品查询spu
                String spuId = sku.getSpuId();
                Result<Spu> spuResult = spuFeign.findById(spuId);
                Spu spu = spuResult.getData();
                //补全购物车订单数据
                OrderItem orderItem = sku2OrderItem(sku, spu, num);
                String jsonString = JSONObject.toJSONString(orderItem);
                //将订单数据保存到redis中
                redisTemplate.boundHashOps("Cart_" + username).put(id,jsonString);
        }

    }

    /***
     * SKU转成OrderItem
     * @param sku
     * @param num
     * @return
     */
    private OrderItem sku2OrderItem(Sku sku,Spu spu,Integer num){
        //补全购物车订单数据
        OrderItem orderItem = new OrderItem();
        //补全分类
        Integer category1Id = spu.getCategory1Id();
        Integer category2Id = spu.getCategory2Id();
        Integer category3Id = spu.getCategory3Id();
        orderItem.setCategoryId1(category1Id);
        orderItem.setCategoryId2(category2Id);
        orderItem.setCategoryId3(category3Id);
        //补全spuid，skuid
        orderItem.setSpuId(spu.getId());
        orderItem.setSkuId(sku.getId());
        //补全商品名称，单价，数量，总金额，图片地址
        orderItem.setName(sku.getName());
        orderItem.setPrice(sku.getPrice());
        orderItem.setNum(num);
        orderItem.setMoney(sku.getPrice()*num);
        orderItem.setImage(sku.getImage());
        return orderItem;
    }


    /*
    * 查询购物车
    * @Param: username
    * @create: 2020/9/6 16:58
    * @return: 14265
    */
    @Override
    public List<OrderItem> findAll(String username){
        List<OrderItem> orderItemList=new ArrayList<>();
        List values =redisTemplate.boundHashOps("Cart_" + username).values();
        if (values!=null){
            for (Object value : values) {
                String string = value.toString();
                OrderItem orderItem = JSONObject.parseObject(string, OrderItem.class);
                orderItemList.add(orderItem);
            }
            return orderItemList;
        }
        return null;
    }
//获取下单的商品
    @Override
    public List<OrderItem> getChoose(String username, String[] ids) {
        List<OrderItem> orderItemList=new ArrayList<>();
        if (ids!=null){
            for (String id : ids) {
                //获取指定数据
                String o = redisTemplate.boundHashOps("Cart_" + username).get(id).toString();
                OrderItem orderItem = JSONObject.parseObject(o, OrderItem.class);
                orderItemList.add(orderItem);

            }
            return orderItemList;
        }
       return null;
    }
}
