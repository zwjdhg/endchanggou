package com.changgou.order.service;
import com.changgou.order.pojo.Order;
import com.github.pagehelper.PageInfo;
import java.util.List;
/****
 * @Author:deng
 * @Description:Order业务层接口
 *****/
public interface OrderService {
	 /***
     * 查找订单：
     * @param orderId
     */
	 HashMap<String, Object> findAllByUsername(Order order);
    /***
     * 支付失败：逻辑删除订单操作
     * @param orderId
     */
    void deleteOrder(String orderId);
    /***
     * 根据订单ID修改订单状态
     * @param transactionid 交易流水号
     * @param orderId
     */
    void updateStatus(String orderId,String transactionid);

    /***
     * Order多条件分页查询
     * @param order
     * @param page
     * @param size
     * @return
     */
    PageInfo<Order> findPage(Order order, int page, int size);

    /***
     * Order分页查询
     * @param page
     * @param size
     * @return
     */
    PageInfo<Order> findPage(int page, int size);

    /***
     * Order多条件搜索方法
     * @param order
     * @return
     */
    List<Order> findList(Order order);

    /***
     * 删除Order
     * @param id
     */
    void delete(String id);

    /***
     * 修改Order数据
     * @param order
     */
    void update(Order order);

    /***
     * 新增Order
     * @param order
     */
    void add(Order order);

    /**
     * 根据ID查询Order
     * @param id
     * @return
     */
     Order findById(String id);

    /***
     * 查询所有Order
     * @return
     */
    List<Order> findAll();
	
	
	HashMap<String, Object> findAllByUsername(Order order);

    /**
     * 同时时间查询
     * @param month
     * @param username
     * @return
     */
    List<Order> findByTime(String month, String username);
}
