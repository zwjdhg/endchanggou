package com.changgou.order.dao;
import com.changgou.order.pojo.OrderLog;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:OrderLog的Dao
 *****/
public interface OrderLogMapper extends Mapper<OrderLog> {
}
