package com.changgou.order.dao;
import com.changgou.order.pojo.OrderItem;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:OrderItem的Dao
 *****/
public interface OrderItemMapper extends Mapper<OrderItem> {
	
	List<OrderItem> selectOrderById(@Param("id") String id);
}
