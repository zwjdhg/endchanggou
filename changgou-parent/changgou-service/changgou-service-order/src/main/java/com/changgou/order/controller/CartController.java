package com.changgou.order.controller;

import com.changgou.order.config.TokenDecode;
import com.changgou.order.pojo.OrderItem;
import com.changgou.order.service.CartService;
import com.changgou.util.Result;
import com.changgou.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: CartController
 * @Author: ZWJ
 * @Date: 2020-09-06 16:30
 * @Synopsis: Null
 **/
@RestController
@RequestMapping(value = "/cart")
public class CartController {
    @Autowired
    private CartService cartService;
    /***
     * 添加购物车
     * @param num :购买商品数量
     * @param id ：购买ID
     * //@param username ：购买用户
     * @return
     */
    @RequestMapping("/add")
    public Result add(Integer num, String id){
        //用户名
        Map<String, String> userInfo = TokenDecode.getUserInfo();
        String username = userInfo.get("username");
        //将商品加入购物车
        cartService.add(num,id,username);
        return new Result(true, StatusCode.OK,"加入购物车成功！");
    }
    /*
     * 查询购物车
     * @Param: username
     * @create: 2020/9/6 16:58
     * @return: 14265
     */
    @GetMapping("/list")
    public Result<List<OrderItem>> findAll(){
        //用户名
        Map<String, String> userInfo = TokenDecode.getUserInfo();
        String username = userInfo.get("username");
        List<OrderItem> orderItems = cartService.findAll(username);
        return new Result(true, StatusCode.OK, "查询购物车成功", orderItems);
    }

    @GetMapping("/list/choose")
    public Result<List<OrderItem>> getChoose(String[] ids){
        //用户名
        Map<String,String> userInfo=TokenDecode.getUserInfo();
        String username = userInfo.get("username");
        List<OrderItem> choose = cartService.getChoose(username, ids);
        return new Result(true, StatusCode.OK, "查询订单商品成功", choose);
    }

}
