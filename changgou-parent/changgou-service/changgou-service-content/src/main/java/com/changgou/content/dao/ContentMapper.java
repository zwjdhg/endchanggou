package com.changgou.content.dao;
import com.changgou.content.pojo.Content;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Content的Dao
 *****/
@Repository
public interface ContentMapper extends Mapper<Content> {
}
