package com.changgou.content.dao;
import com.changgou.content.pojo.ContentCategory;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:ContentCategory的Dao
 *****/
@Repository
public interface ContentCategoryMapper extends Mapper<ContentCategory> {
}
