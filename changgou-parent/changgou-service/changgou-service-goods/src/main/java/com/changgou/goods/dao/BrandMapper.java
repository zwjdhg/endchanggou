package com.changgou.goods.dao;
import com.changgou.goods.pojo.Brand;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/****
 * @Author:deng
 * @Description:Brand的Dao
 *****/
@Repository
public interface BrandMapper extends Mapper<Brand> {
    /***
     * 根据分类ID查询品牌集合
     * @param categoryid :分类ID
     * @return
     */
    @Select("SELECT b.* FROM tb_category_brand cb, tb_brand b " +
            "WHERE cb.brand_id=b.id " +
            "and cb.category_id=#{categoryid}")
    List<Brand> findByCategoryId(Integer categoryid);
}
