package com.changgou.goods.dao;
import com.changgou.goods.pojo.Spec;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Spec的Dao
 *****/
@Repository
public interface SpecMapper extends Mapper<Spec> {
}
