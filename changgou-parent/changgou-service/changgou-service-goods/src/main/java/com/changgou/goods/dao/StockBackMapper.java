package com.changgou.goods.dao;
import com.changgou.goods.pojo.StockBack;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:StockBack的Dao
 *****/
@Repository
public interface StockBackMapper extends Mapper<StockBack> {
}
