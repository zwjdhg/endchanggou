package com.changgou.goods.service;

import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/****
 * @Author:deng
 * @Description:Spu业务层接口
 *****/
public interface SpuService {

    /****
     * 销量更新实现
     */
    int decrCount(@RequestParam Map<String,String> map);
    /***
     * 逻辑删除
     * @param spuId
     */
    void logicDelete(String spuId);
    /**
     * 恢复数据
     * @param id
     * @return
     */
    void restore(String id);

    /***
     * 批量下架
     * @param ids:需要下架的商品ID集合
     * @return
     */
   int pullMany(String[] ids);
    /***
     * 批量上架
     * @param ids:需要上架的商品ID集合
     * @return
     */
    int putMany(String[] ids);

    /***
     * 商品上架
     * @param spuId
     */
    void put(String spuId);
    /***
     * 商品下架
     * @param spuId
     */
    void pull(String spuId);
    /***
     * 商品审核
     * @param spuId
     */
    void audit(String spuId);
    /***
     * 根据SPU的ID查找SPU以及对应的SKU集合
     * @param id
     * @return
     */
    Goods findGoodsById(String id);
    /***
     * 添加商品
     * @param goods
     * @return
     */
void saveGoods(Goods goods);

    /***
     * Spu多条件分页查询
     * @param spu
     * @param page
     * @param size
     * @return
     */
    PageInfo<Spu> findPage(Spu spu, int page, int size);

    /***
     * Spu分页查询
     * @param page
     * @param size
     * @return
     */
    PageInfo<Spu> findPage(int page, int size);

    /***
     * Spu多条件搜索方法
     * @param spu
     * @return
     */
    List<Spu> findList(Spu spu);

    /***
     * 删除Spu
     * @param id
     */
    void delete(String id);

    /***
     * 修改Spu数据
     * @param spu
     */
    void update(Spu spu);

    /***
     * 新增Spu
     * @param spu
     */
    void add(Spu spu);

    /**
     * 根据ID查询Spu
     * @param id
     * @return
     */
     Spu findById(String id);

    /***
     * 查询所有Spu
     * @return
     */
    List<Spu> findAll();
}
