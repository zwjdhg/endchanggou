package com.changgou.goods.dao;
import com.changgou.goods.pojo.Sku;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/****
 * @Author:deng
 * @Description:Sku的Dao
 *****/
@Repository
public interface SkuMapper extends Mapper<Sku> {
    /****
     * 库存递减实现
     * @param id,  num
     */
    @Update("UPDATE tb_sku SET num=num-#{num},update_time=NOW(),sale_num=sale_num+#{num} WHERE num>=#{num} and id=#{id}")
    int decrCount(String id, Integer num);
}
