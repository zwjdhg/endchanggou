package com.changgou.goods.dao;
import com.changgou.goods.pojo.Album;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Album的Dao
 *****/
@Repository
public interface AlbumMapper extends Mapper<Album> {
}
