package com.changgou.goods.dao;
import com.changgou.goods.pojo.CategoryBrand;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:CategoryBrand的Dao
 *****/
@Repository
public interface CategoryBrandMapper extends Mapper<CategoryBrand> {
}
