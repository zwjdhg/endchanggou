package com.changgou.goods.dao;
import com.changgou.goods.pojo.Spu;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Spu的Dao
 *****/
@Repository
public interface SpuMapper extends Mapper<Spu> {
    /****
     * 库存递减实现
     * @param id,  num
     */
    @Update("UPDATE tb_spu SET sale_num=sale_num+#{num} WHERE id=#{id}")
    int decrCount(String id, Integer num);
}
