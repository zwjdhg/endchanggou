package com.changgou.goods.dao;
import com.changgou.goods.pojo.Category;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Category的Dao
 *****/
@Repository
public interface CategoryMapper extends Mapper<Category> {
}
