package com.changgou.goods.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.dao.CategoryMapper;
import com.changgou.goods.dao.SkuMapper;
import com.changgou.goods.dao.SpuMapper;
import com.changgou.goods.pojo.*;
import com.changgou.goods.service.SpuService;
import com.changgou.util.IdWorker;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/****
 * @Author:deng
 * @Description:Spu业务层接口实现类
 *****/
@Service
public class SpuServiceImpl implements SpuService {

    @Autowired
    private SpuMapper spuMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private BrandMapper brandMapper;
    @Autowired
    private SkuMapper skuMapper;

    /****
     * 销量更新实现
     * @param map
     */
    @Override
    public int decrCount(Map<String, String> map) {
        if (map == null) {
            throw new RuntimeException("spu库存更新失败");
        }
        int a=0;
            for (Map.Entry<String, String> stringIntegerEntry : map.entrySet()) {
                //获取spu的id
                String key = stringIntegerEntry.getKey();
                //获取销量
                Integer value = Integer.parseInt(stringIntegerEntry.getValue());
                //更新数据库
                a = spuMapper.decrCount(key, value);
                if (a<=0){
                    throw new RuntimeException("spu库存更新失败");
                }

            }

        //没有进行更新返回-1
        return a;
    }

    /***
     * 逻辑删除
     * @param spuId
     */
    @Override
    public void logicDelete(String spuId) {
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        if (!("0".equals(spu.getIsMarketable()))) {
            throw new RuntimeException("该商品未下架不可删除");
        }
        spu.setStatus("0");
        spu.setIsDelete("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /**
     * 恢复数据
     *
     * @param id
     * @return
     */
    @Override
    public void restore(String id) {
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        if (!"1".equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品未被删除，无需恢复");
        }
        //未被删除
        spu.setIsDelete("0");
        //未审核
        spu.setStatus("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /***
     * 批量下架
     * @param ids :需要下架的商品ID集合
     * @return
     */
    @Override
    public int pullMany(String[] ids) {
        //设置上架状态
        Spu spu = new Spu();
        spu.setIsMarketable("0");
        //设置满足上架的条件
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        //设置那些spu可以上架
        criteria.andIn("id", Arrays.asList(ids));
        //上架的条件需要没有被删除
        criteria.andEqualTo("isDelete", "0");
        //已经上架的
        criteria.andEqualTo("isMarketable", "1");
        int i = spuMapper.updateByExampleSelective(spu, example);
        return i;
    }

    /***
     * 批量上架
     * @param ids :需要上架的商品ID集合
     * @return
     */
    @Override
    public int putMany(String[] ids) {
        //设置上架状态
        Spu spu = new Spu();
        spu.setIsMarketable("1");
        //设置满足上架的条件
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        //设置那些spu可以上架
        criteria.andIn("id", Arrays.asList(ids));
        //上架的条件需要没有被删除
        criteria.andEqualTo("isDelete", "0");
        //条件;状态必须是已审核
        criteria.andEqualTo("status", "1");
        //已经下架的
        criteria.andEqualTo("isMarketable", "0");
        int i = spuMapper.updateByExampleSelective(spu, example);
        return i;
    }

    /***
     * 商品上架
     * @param spuId
     */
    @Override
    public void put(String spuId) {
//获取spu信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        if ("1".equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品已经被删除，无法进行操作");
        }
        //判断是否已经被上架
        if ("1".equals(spu.getIsMarketable())) {
            throw new RuntimeException("该商品已经被上架，无需重复上架");
        }
        //判断是否已经通过审核
        if (!("1".equals(spu.getStatus()))) {
            throw new RuntimeException("该商品未通过审核，不能进行上架");
        }
        //修改上架信息
        spu.setIsMarketable("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /***
     * 商品下架
     * @param spuId
     */
    @Override
    public void pull(String spuId) {
        //获取spu信息
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        //判断是否被删除
        if (spu.getIsDelete().equals("1")) {
            throw new RuntimeException("该商品已经被删除，无法进行操作");
        }
        //判断是否已经被下架
        if (spu.getIsMarketable().equals("0")) {
            throw new RuntimeException("该商品已经被下架，无法进行操作");
        }
        //修改下架信息，//以及审核信息
        spu.setIsMarketable("0");
        //spu.setStatus("0");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /***
     * 商品审核
     * @param spuId
     */
    @Override
    public void audit(String spuId) {
        Spu spu = spuMapper.selectByPrimaryKey(spuId);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        //判断该商品是否被删除
        if ("1".equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品已经被删除");
        }
        //修改上架状态以及审核状态
        spu.setIsMarketable("1");
        spu.setStatus("1");
        spuMapper.updateByPrimaryKeySelective(spu);
    }

    /***
     * 根据SPU的ID查找SPU以及对应的SKU集合
     * @param id
     * @return
     */
    @Override
    public Goods findGoodsById(String id) {
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null) {
            throw new RuntimeException("数据你猜在不在？");
        }
        Sku sku = new Sku();
        sku.setSpuId(spu.getId());
        List<Sku> skus = skuMapper.select(sku);
        Goods goods = new Goods();
        goods.setSpu(spu);
        if (skus != null) {
            goods.setSkuList(skus);
        }
        return goods;
    }

    /***
     * 添加商品
     * @param goods
     * @return
     */
    @Override
    public void saveGoods(Goods goods) {
        //获取spu的信息
        Spu spu = goods.getSpu();

        //获取品牌信息
        Integer brandId = spu.getBrandId();
        Brand brand = brandMapper.selectByPrimaryKey(brandId);
        //获取模板信息
        Integer category3Id = spu.getCategory3Id();
        Category category = categoryMapper.selectByPrimaryKey(category3Id);

        //补全spu信息
        //补全spu模板信息
        spu.setTemplateId(category.getTemplateId());
        if (StringUtils.isEmpty(spu.getId())) {
            //补全spuid
            spu.setId("N" + new IdWorker().nextId());
            //将spu数据保存
            spuMapper.insertSelective(spu);
        } else {
            //当有id是进行修改时则以修改的方式进行将spu数据保存
            spuMapper.updateByPrimaryKeySelective(spu);
            //将之前的所有与spu有关联的sku进行删除
            /*Sku sku=new Sku();
            sku.setSpuId(spu.getId());
            skuMapper.delete(sku);*/
            Example example = new Example(Sku.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("spuId", spu.getId());
            skuMapper.deleteByExample(example);

        }

        //获取sku
        List<Sku> skuList = goods.getSkuList();
        //补全数据
        for (Sku sku : skuList) {

            if (StringUtils.isEmpty(sku.getSpec())) {
                sku.setSpec("{}");
            }
            //补全sku名字
            String name = spu.getName();
            //将json格式的字符串转换成map
            Map<String, String> map = JSONObject.parseObject(sku.getSpec(), Map.class);
            //将map里面的数据进行一个一个取出
            Set<Map.Entry<String, String>> entries = map.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                String value = entry.getValue();
                //进行名字拼接
                name = name + " " + value;
            }
            sku.setName(name);
            //补全id
            sku.setId("S" + new IdWorker().nextId());
            //补全create-time
            sku.setCreateTime(new Date());
            //补全update-time
            sku.setUpdateTime(new Date());
            //补全spu-id
            sku.setSpuId(spu.getId());
            //补全category-id
            sku.setCategoryId(category3Id);
            //补全category-name
            sku.setCategoryName(category.getName());
            //补全brand-name
            sku.setBrandName(brand.getName());
            //将sku数据保存
            skuMapper.insertSelective(sku);
        }
    }

    /**
     * Spu条件+分页查询
     *
     * @param spu  查询条件
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @Override
    public PageInfo<Spu> findPage(Spu spu, int page, int size) {
        //分页
        PageHelper.startPage(page, size);
        //搜索条件构建
        Example example = createExample(spu);
        //执行搜索
        return new PageInfo<Spu>(spuMapper.selectByExample(example));
    }

    /**
     * Spu分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageInfo<Spu> findPage(int page, int size) {
        //静态分页
        PageHelper.startPage(page, size);
        //分页查询
        return new PageInfo<Spu>(spuMapper.selectAll());
    }

    /**
     * Spu条件查询
     *
     * @param spu
     * @return
     */
    @Override
    public List<Spu> findList(Spu spu) {
        //构建查询条件
        Example example = createExample(spu);
        //根据构建的条件查询数据
        return spuMapper.selectByExample(example);
    }


    /**
     * Spu构建查询对象
     *
     * @param spu
     * @return
     */
    public Example createExample(Spu spu) {
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        if (spu != null) {
            // 主键
            if (!StringUtils.isEmpty(spu.getId())) {
                criteria.andEqualTo("id", spu.getId());
            }
            // 货号
            if (!StringUtils.isEmpty(spu.getSn())) {
                criteria.andEqualTo("sn", spu.getSn());
            }
            // SPU名
            if (!StringUtils.isEmpty(spu.getName())) {
                criteria.andLike("name", "%" + spu.getName() + "%");
            }
            // 副标题
            if (!StringUtils.isEmpty(spu.getCaption())) {
                criteria.andEqualTo("caption", spu.getCaption());
            }
            // 品牌ID
            if (!StringUtils.isEmpty(spu.getBrandId())) {
                criteria.andEqualTo("brandId", spu.getBrandId());
            }
            // 一级分类
            if (!StringUtils.isEmpty(spu.getCategory1Id())) {
                criteria.andEqualTo("category1Id", spu.getCategory1Id());
            }
            // 二级分类
            if (!StringUtils.isEmpty(spu.getCategory2Id())) {
                criteria.andEqualTo("category2Id", spu.getCategory2Id());
            }
            // 三级分类
            if (!StringUtils.isEmpty(spu.getCategory3Id())) {
                criteria.andEqualTo("category3Id", spu.getCategory3Id());
            }
            // 模板ID
            if (!StringUtils.isEmpty(spu.getTemplateId())) {
                criteria.andEqualTo("templateId", spu.getTemplateId());
            }
            // 运费模板id
            if (!StringUtils.isEmpty(spu.getFreightId())) {
                criteria.andEqualTo("freightId", spu.getFreightId());
            }
            // 图片
            if (!StringUtils.isEmpty(spu.getImage())) {
                criteria.andEqualTo("image", spu.getImage());
            }
            // 图片列表
            if (!StringUtils.isEmpty(spu.getImages())) {
                criteria.andEqualTo("images", spu.getImages());
            }
            // 售后服务
            if (!StringUtils.isEmpty(spu.getSaleService())) {
                criteria.andEqualTo("saleService", spu.getSaleService());
            }
            // 介绍
            if (!StringUtils.isEmpty(spu.getIntroduction())) {
                criteria.andEqualTo("introduction", spu.getIntroduction());
            }
            // 规格列表
            if (!StringUtils.isEmpty(spu.getSpecItems())) {
                criteria.andEqualTo("specItems", spu.getSpecItems());
            }
            // 参数列表
            if (!StringUtils.isEmpty(spu.getParaItems())) {
                criteria.andEqualTo("paraItems", spu.getParaItems());
            }
            // 销量
            if (!StringUtils.isEmpty(spu.getSaleNum())) {
                criteria.andEqualTo("saleNum", spu.getSaleNum());
            }
            // 评论数
            if (!StringUtils.isEmpty(spu.getCommentNum())) {
                criteria.andEqualTo("commentNum", spu.getCommentNum());
            }
            // 是否上架,0已下架，1已上架
            if (!StringUtils.isEmpty(spu.getIsMarketable())) {
                criteria.andEqualTo("isMarketable", spu.getIsMarketable());
            }
            // 是否启用规格
            if (!StringUtils.isEmpty(spu.getIsEnableSpec())) {
                criteria.andEqualTo("isEnableSpec", spu.getIsEnableSpec());
            }
            // 是否删除,0:未删除，1：已删除
            if (!StringUtils.isEmpty(spu.getIsDelete())) {
                criteria.andEqualTo("isDelete", spu.getIsDelete());
            }
            // 审核状态，0：未审核，1：已审核，2：审核不通过
            if (!StringUtils.isEmpty(spu.getStatus())) {
                criteria.andEqualTo("status", spu.getStatus());
            }
        }
        return example;
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(String id) {
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if (spu == null) {
            throw new RuntimeException("该商品不存在，无法进行操作");
        }
        if (!"1".equals(spu.getIsDelete())) {
            throw new RuntimeException("该商品为被逻辑删除，不可直接物理删除");
        }
        spuMapper.deleteByPrimaryKey(id);
    }

    /**
     * 修改Spu
     *
     * @param spu
     */
    @Override
    public void update(Spu spu) {
        spuMapper.updateByPrimaryKey(spu);
    }

    /**
     * 增加Spu
     *
     * @param spu
     */
    @Override
    public void add(Spu spu) {
        spuMapper.insert(spu);
    }

    /**
     * 根据ID查询Spu
     *
     * @param id
     * @return
     */
    @Override
    public Spu findById(String id) {
        return spuMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询Spu全部数据
     *
     * @return
     */
    @Override
    public List<Spu> findAll() {
        return spuMapper.selectAll();
    }
}
