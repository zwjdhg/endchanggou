package com.changgou.goods.dao;
import com.changgou.goods.pojo.Para;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Para的Dao
 *****/
@Repository
public interface ParaMapper extends Mapper<Para> {
}
