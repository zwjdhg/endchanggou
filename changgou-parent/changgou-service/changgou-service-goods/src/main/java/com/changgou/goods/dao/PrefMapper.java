package com.changgou.goods.dao;
import com.changgou.goods.pojo.Pref;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:Pref的Dao
 *****/
@Repository
public interface PrefMapper extends Mapper<Pref> {
}
