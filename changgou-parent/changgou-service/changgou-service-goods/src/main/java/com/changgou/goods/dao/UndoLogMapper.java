package com.changgou.goods.dao;
import com.changgou.goods.pojo.UndoLog;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:UndoLog的Dao
 *****/
@Repository
public interface UndoLogMapper extends Mapper<UndoLog> {
}
