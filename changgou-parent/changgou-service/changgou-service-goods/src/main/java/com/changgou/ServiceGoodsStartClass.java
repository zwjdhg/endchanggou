package com.changgou;

import com.changgou.util.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @ClassName: ServiceGoodsStartClass
 * @Author: ZWJ
 * @Date: 2020-08-23 17:11
 * @Synopsis: Null
 **/
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = "com.changgou.goods.dao")
public class ServiceGoodsStartClass {
    public static void main(String[] args) {
        SpringApplication.run(ServiceGoodsStartClass.class,args);
    }
    /*
    * @Description: id 生成的类
    */
    @Bean
    public IdWorker idWorker(){
        return new IdWorker(0,1);
    }
}
