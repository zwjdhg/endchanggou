package com.changgou.seckill.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: seckillOrderMQ
 * @Author: ZWJ
 * @Date: 2020-09-11 19:56
 * @Synopsis: Null
 **/
@Configuration
public class seckillOrderMQ {
    //交换机
    @Bean("seckillOrderExchange")
    public Exchange getExchange(){
        return ExchangeBuilder.directExchange("seckillOrderExchange").build();
    }
    @Bean("seckillOrderQueue")
    public Queue getQueue(){
     return QueueBuilder.durable("seckillOrderQueue").build();
    }
    @Bean("seckillOrderBinding")
    public Binding getBinding(@Qualifier("seckillOrderExchange") Exchange exchange,
                              @Qualifier("seckillOrderQueue") Queue queue){
        return BindingBuilder.bind(queue).to(exchange).with("seckillOrderQueue").noargs();
    }
}
