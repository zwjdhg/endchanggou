package com.changgou.seckill.dao;
import com.changgou.seckill.pojo.SeckillOrder;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:SeckillOrder的Dao
 *****/
@Repository
public interface SeckillOrderMapper extends Mapper<SeckillOrder> {
}
