package com.changgou.seckill.dao;
import com.changgou.seckill.pojo.SeckillGoods;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/****
 * @Author:deng
 * @Description:SeckillGoods的Dao
 *****/
@Repository
public interface SeckillGoodsMapper extends Mapper<SeckillGoods> {
}
