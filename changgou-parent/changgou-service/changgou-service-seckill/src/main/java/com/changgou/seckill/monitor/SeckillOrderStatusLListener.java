package com.changgou.seckill.monitor;

import com.alibaba.fastjson.JSONObject;
import com.changgou.seckill.service.SeckillOrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jnlp.UnavailableServiceException;
import java.util.Map;

/**
 * @ClassName: SeckillOrderStatusLListener
 * @Author: ZWJ
 * @Date: 2020-09-13 21:05
 * @Synopsis: Null
 **/
@Component
public class SeckillOrderStatusLListener {

    @Autowired
    private SeckillOrderService seckillOrderService;

    //秒杀订单支付状态的监听
    @RabbitListener(queues = "queueSeckillOrder")
    public void getStatus(String message){
        Map<String,String> resultMap = JSONObject.parseObject(message, Map.class);
        if ("SUCCESS".equals(resultMap.get("return_code"))){
            String attach = resultMap.get("attach");
            Map<String,String> map = JSONObject.parseObject(attach, Map.class);
            String username = map.get("username");
            //返回码正常
            if ("SUCCESS".equals(resultMap.get("result_code"))){

                //业务结果正常，支付成功
                //有订单号等详细信息返回,支付成功，修改订单状态
                String transaction_id = resultMap.get("transaction_id");
                //修改订单信息
                seckillOrderService.updatePayStatus(username,transaction_id);
            }else {
                //支付失败
                seckillOrderService.closeOrder(username);

            }
        }
        //支付失败
    }
}
