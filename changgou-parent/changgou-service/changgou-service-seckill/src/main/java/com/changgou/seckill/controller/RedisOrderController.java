package com.changgou.seckill.controller;

import com.alibaba.fastjson.JSONObject;
import com.changgou.seckill.pojo.SeckillOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: RedisOrderController
 * @Author: ZWJ
 * @Date: 2020-09-13 19:53
 * @Synopsis: Null
 **/
@RestController
@RequestMapping("/order")
public class RedisOrderController {
@Autowired
private StringRedisTemplate stringRedisTemplate;
    @GetMapping("/status")
    public SeckillOrder findByName(String name){
        String username="szitheima";
        Object o = stringRedisTemplate.boundHashOps("SeckillOrder").get(username);
        SeckillOrder seckillOrder = JSONObject.parseObject(o.toString(), SeckillOrder.class);
        return seckillOrder;
    }
}
