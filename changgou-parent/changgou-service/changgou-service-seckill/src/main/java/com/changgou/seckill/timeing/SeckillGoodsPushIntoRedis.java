package com.changgou.seckill.timeing;

import com.alibaba.fastjson.JSONObject;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.pojo.SeckillGoods;
import com.changgou.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @ClassName: SeckillGoodsPushIntoRedis
 * @Author: ZWJ
 * @Date: 2020-09-11 16:20
 * @Synopsis: Null
 **/
@Component
public class SeckillGoodsPushIntoRedis {
@Autowired
private SeckillGoodsMapper seckillGoodsMapper;
@Autowired
private RedisTemplate redisTemplate;
    /****
     * 每30秒执行一次
     * 将符合参与秒杀的商品查询出来再存入到Redis缓存
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void loadGoodsPushRedis(){
        //参与秒杀的时段
        List<Date> dateMenus = DateUtil.getDateMenus();
        for (Date dateMenu : dateMenus) {
            //存入redis商品的key
            String key = DateUtil.data2str(dateMenu, DateUtil.PATTERN_YYYYMMDDHH);
            //秒杀开始时间
            String startDate = DateUtil.data2str(dateMenu, DateUtil.PATTERN_YYYY_MM_DDHHMM);
            //秒杀结束时间
            String endDate = DateUtil.data2str(DateUtil.addDateHour(dateMenu, 2), DateUtil.PATTERN_YYYY_MM_DDHHMM);
            //设置搜索条件
            Example example=new Example(SeckillGoods.class);
            Example.Criteria criteria = example.createCriteria();
            //审核通过
            criteria.andEqualTo("status","1");
            //库存大于0
            criteria.andGreaterThan("stockCount",0);
            //大于等于开始时间
            criteria.andGreaterThanOrEqualTo("startTime",startDate);
            //实际小于结束时间，这里是因为数据库数据的原因是小于等于
            criteria.andLessThanOrEqualTo("endTime",endDate);
            //查询redis中没有的秒杀商品
            Set<Object> keys = redisTemplate.boundHashOps("SeckillGoods_" + key).keys();
            if (keys!=null&&keys.size()>0){
                criteria.andNotIn("id",keys);
            }
            //执行查询获取结果
            List<SeckillGoods> seckillGoods = seckillGoodsMapper.selectByExample(example);
            if (seckillGoods!=null &&seckillGoods.size()>0){
                //将数据存入redis
                for (SeckillGoods seckillGood : seckillGoods) {
                    //商品的id
                    String id = seckillGood.getId();
                    redisTemplate.boundHashOps("SeckillGoods_"+key).put(seckillGood.getId(),seckillGood);
                    //获取数组，商品库存多少，数组长度就多少
                    String[] string = getString(id, seckillGood.getStockCount());
                    //生成一个按照商品库存数量长度的队列:左进右出
                    redisTemplate.boundListOps("SeckillGoodsStocKQueue_"+seckillGood.getId()).leftPushAll(string);
                    //用redis来表示商品的库存的总量
                    redisTemplate.boundHashOps("SeckillGoodsStockCount").increment(seckillGood.getId(),seckillGood.getStockCount());
                }
            }

        }
    }
    //设置商品库存变成队列
    public String[] getString(String id,Integer count){
        String[] strings=new String[count];
        for (int i=0;i<count;i++){
            strings[i]=id;
        }
        return strings;
    }
}
