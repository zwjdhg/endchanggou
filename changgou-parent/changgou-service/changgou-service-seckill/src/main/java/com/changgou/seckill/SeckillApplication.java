package com.changgou.seckill;

import com.changgou.entity.FeignInterceptor;
import com.changgou.util.IdWorker;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @ClassName: SeckillApplication
 * @Author: ZWJ
 * @Date: 2020-09-11 15:35
 * @Synopsis: Null
 **/
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.changgou.seckill.dao"})
@EnableFeignClients
@EnableScheduling
public class SeckillApplication {
    public static void main(String[] args){
        SpringApplication.run(SeckillApplication.class,args);
    }
    //生成随机字符用于生成主键id
    @Bean
    public IdWorker idWorker(){
        return new IdWorker(2,1);
    }
    //服务与服务之间feign调用给请求添加（补充）数据
    @Bean
    public FeignInterceptor feignInterceptor(){
        return new FeignInterceptor();
    }
}
