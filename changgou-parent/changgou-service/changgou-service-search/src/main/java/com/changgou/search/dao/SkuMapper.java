package com.changgou.search.dao;

import com.changgou.search.pojo.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: SearchMapper
 * @Author: ZWJ
 * @Date: 2020-08-29 16:17
 * @Synopsis: Null
 **/
@Repository
public interface SkuMapper extends ElasticsearchRepository<SkuInfo,String> {
}
