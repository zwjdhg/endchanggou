package com.changgou.search.controller;

import com.changgou.search.pojo.SkuInfo;
import com.changgou.search.service.Impl.DemoServiceImpl;
import com.changgou.search.service.SkuService;
import com.changgou.util.Result;
import com.changgou.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SearchController
 * @Author: ZWJ
 * @Date: 2020-08-29 16:19
 * @Synopsis: Null
 **/
@RestController
@RequestMapping("/search")
@CrossOrigin
public class SkuController {
    @Autowired
    private SkuService skuService;
    @Autowired
    private DemoServiceImpl demoService;
    /***
     * 调用搜索实现
     */
    @GetMapping
    public Result<Map> search(@RequestParam(required = false) Map<String,String> searchMap) throws Exception{
        Map search = skuService.search(searchMap);
       // Map search = demoService.search01(searchMap);
        return  new Result(true, StatusCode.OK,"调用搜索实现成功",search);
    }
    /***
     * 导入SKU数据
     */
    @GetMapping("/import")
    public Result importSku() {
        //导入sku数据
        skuService.importSku();
        return new Result(true, StatusCode.OK,"导入SKU数据成功");
    }
}
