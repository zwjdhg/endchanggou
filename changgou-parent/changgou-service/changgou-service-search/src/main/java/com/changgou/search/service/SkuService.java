package com.changgou.search.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Map;

/**
 * @ClassName: SkuService
 * @Author: ZWJ
 * @Date: 2020-08-29 16:14
 * @Synopsis: Null
 **/
public interface SkuService {
    /***
     * 搜索
     * @param searchMap
     * @return
     */
    Map search(Map<String, String> searchMap);


    /***
     * 导入SKU数据
     */
    void importSku() ;
}
