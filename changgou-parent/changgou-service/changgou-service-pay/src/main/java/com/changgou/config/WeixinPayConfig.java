package com.changgou.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: WeixinPayConfig
 * @Author: ZWJ
 * @Date: 2020-09-09 13:03
 * @Synopsis: Null
 **/
@Configuration
public class WeixinPayConfig {
    //交换机名字
    @Value("${mq.pay.exchange.order}")
    private String exchange;
    //队列名字
    @Value("${mq.pay.queue.order}")
    private String queue;
    //路由
    @Value("${mq.pay.routing.key}")
    private String key;
    //创建交换机
    @Bean("exchangeOrder")
    public Exchange getExchange() {
        //定向模式交换机
        return ExchangeBuilder.directExchange(exchange).build();
    }

    //队列
    @Bean("queueOrder")
    public Queue getQueue() {
        return QueueBuilder.durable(queue).build();
    }

    //绑定交换机与队列
    @Bean("bindOrder")
    public Binding getBinding(@Qualifier("exchangeOrder") Exchange exchange
            , @Qualifier("queueOrder") Queue queue) {
        //将交换机与队列进行绑定并且进行设置路由
        return BindingBuilder.bind(queue).to(exchange).with(key).noargs();
    }

    //秒杀支付状态队列
    @Bean("queueSeckillOrder")
    public Queue getSeckillOrder(){
        return QueueBuilder.durable("queueSeckillOrder").build();
    }
//将秒杀状队列绑定交换机
    public Binding getSeckillOrderBinding(@Qualifier("queueSeckillOrder") Queue queue,
                                          @Qualifier("exchangeOrder") Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("queueSeckillOrder").noargs();
    }
}
