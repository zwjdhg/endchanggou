package com.changgou.service;

import java.util.Map;

/**
 * @ClassName: WeixinPayService
 * @Author: ZWJ
 * @Date: 2020-09-08 20:23
 * @Synopsis: Null
 **/
public interface WeixinPayService {
    /*****
     * 创建二维码
     * dataMap中有
     * out_trade_no : 客户端自定义订单编号
     * total_fee    : 交易金额,单位：分
     * @return
     */
    Map createNative(Map<String, String> dataMap) ;

    /***
     * 查询订单状态
     * @param out_trade_no : 客户端自定义订单编号
     * @return
     */
    Map queryPayStatus(String out_trade_no);
}
