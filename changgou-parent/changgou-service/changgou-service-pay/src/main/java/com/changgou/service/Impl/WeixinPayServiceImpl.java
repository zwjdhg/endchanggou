package com.changgou.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.changgou.service.WeixinPayService;
import com.changgou.util.HttpClient;
import com.github.wxpay.sdk.WXPayUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: WeixinPayServiceImpl
 * @Author: ZWJ
 * @Date: 2020-09-08 20:27
 * @Synopsis: Null
 **/
@Service
public class WeixinPayServiceImpl implements WeixinPayService {
    @Value("${weixin.appid}")
    private String appid;
    @Value("${weixin.partner}")
    private String partner;
    @Value("${weixin.partnerkey}")
    private String partnerkey;
    @Value("${weixin.notifyurl}")
    private String notifyurl;


    /*****
     * 创建二维码(将交换机和路由写在附加参数上，等到接受微信服务发送回来的数据时，附加参数也一起发送回来了
     * 这样就可以根据发送回来的附加参数实现动态发送信息，不同的订单往不同队列发送消息)
     * dataMap
     * out_trade_no : 客户端自定义订单编号
     * total_fee    : 交易金额,单位：分
     * @return
     */
    @Override
    public Map createNative(Map<String, String> dataMap) {
        try {
            //准备发送的信息
            Map map = new HashMap();
            map.put("appid", appid); //应用ID
            map.put("mch_id", partner);   //商户ID号
            map.put("nonce_str", WXPayUtil.generateNonceStr());   //随机数
            map.put("body", "商品支付");     //订单描述
            map.put("out_trade_no", dataMap.get("outtradeno"));        //商户订单号
            map.put("total_fee", dataMap.get("totalfee"));      //交易金额
            map.put("spbill_create_ip", "127.0.0.1");     //终端IP
            map.put("notify_url", notifyurl);     //回调地址
            map.put("trade_type", "NATIVE"); //交易类型
            //----------------------
            //队列的参数名字
            Map<String,String> attachMap=new HashMap<>();
            attachMap.put("queue",dataMap.get("queue"));
            attachMap.put("exchange",dataMap.get("exchange"));
            //用户名
            String username = dataMap.get("username");
            if (!StringUtils.isEmpty(username)){
                attachMap.put("username",username);
            }
            String jsonString = JSONObject.toJSONString(attachMap);
            //将数据放进
            map.put("attach",jsonString);
            //-----------------------
            //设置发送信息的客户端
            String xml = WXPayUtil.generateSignedXml(map, partnerkey);
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            client.setHttps(true);
            client.setXmlParam(xml);
            client.post();
            String content = client.getContent();
            Map<String, String> map1 = WXPayUtil.xmlToMap(content);
            return map1;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 查询订单状态
     * @param out_trade_no : 客户端自定义订单编号
     * @return
     */
    @Override
    public Map queryPayStatus(String out_trade_no) {
        try {
            //设置查询订单的请求参数
            Map map = new HashMap();
            map.put("appid", appid); //公众账号id
            map.put("mch_id", partner);//商户号
            map.put("out_trade_no", out_trade_no);//商户订单号
            map.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
            //设置发给请求客户端
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            //是否是https请求
            client.setHttps(true);
            //将参数设置进去
            client.setXmlParam(WXPayUtil.generateSignedXml(map, partnerkey));
            //post方式请求
            client.post();
            //接受发送请求后的数据
            String content = client.getContent();
            //通过工具类将接收到xml格式的字符串转换为map
            return WXPayUtil.xmlToMap(content);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
