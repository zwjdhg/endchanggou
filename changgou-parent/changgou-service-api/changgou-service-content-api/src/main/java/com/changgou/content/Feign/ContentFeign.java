package com.changgou.content.Feign;



import com.changgou.content.pojo.Content;
import com.changgou.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @ClassName: ContentFeign
 * @Author: ZWJ
 * @Date: 2020-08-28 17:24
 * @Synopsis: Null
 **/
@FeignClient(value = "server-content")
@RequestMapping("/content")
public interface ContentFeign {
    @GetMapping(value = "/list/category/{id}")
    public Result<List<Content>> findCategoryId(@PathVariable("id") Long id);
}
