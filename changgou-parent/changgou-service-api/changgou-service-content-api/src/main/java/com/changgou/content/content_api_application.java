package com.changgou.content;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @ClassName: content_api_application
 * @Author: ZWJ
 * @Date: 2020-08-28 17:28
 * @Synopsis: Null
 **/
/*@EnableEurekaClient
@SpringBootApplication*/
public class content_api_application {
    public static void main(String[] args) {
        SpringApplication.run(content_api_application.class,args);
    }
}
