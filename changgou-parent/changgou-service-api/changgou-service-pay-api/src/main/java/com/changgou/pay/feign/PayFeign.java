package com.changgou.pay.feign;

import com.changgou.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @ClassName: PayFeign
 * @Author: ZWJ
 * @Date: 2020-09-09 20:22
 * @Synopsis: Null
 **/
@FeignClient("pay")
@RequestMapping(value = "/weixin/pay")
public interface PayFeign {
    /***
     * 查询支付状态
     * @param outtradeno
     * @return
     */
    @GetMapping(value = "/status/query")
    public Result<Map<String, String>> queryStatus(@RequestParam String outtradeno);
}
