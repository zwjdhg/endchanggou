package com.changgou.search.pojo;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @ClassName: SkuInfo
 * @Author: ZWJ
 * @Date: 2020-08-29 14:44
 * @Synopsis: Null
 **/

@Document(indexName = "skuindex", type = "skuinfo")
public class SkuInfo implements Serializable {
    @Id
    private String id;//商品id
    @Field(type = FieldType.Text, analyzer = "ik_smart")
    private String sn;//商品条码
    @Field(type = FieldType.Text, analyzer = "ik_max_word"
            , searchAnalyzer = "ik_max_word")
    private String name;//SKU名称
    @Field(type = FieldType.Double)
    private Long price;//价格（分）
    //@Field(type = FieldType.Integer)
    private Integer num;//库存数量
    //@Field(type = FieldType.Integer)
    private Integer alertNum;//库存预警数量
    //@Field(type = FieldType.Text)
    private String image;//商品图片
    //@Field(type = FieldType.Text)
    private String images;//商品图片列表
    //@Field(type = FieldType.Integer)
    private Integer weight;//重量（克）
    //@Field(type = FieldType.Date)
    private Date createTime;//创建时间
    //@Field(type = FieldType.Date)
    private Date updateTime;//更新时间
    //@Field(type = FieldType.Text)
    private String spuId;//SPUID
    //@Field(type = FieldType.Integer)
    private Integer categoryId;//类目ID
    @Field(type = FieldType.Keyword)
    private String categoryName;//类目名称
    @Field(type = FieldType.Keyword)
    private String brandName;//品牌名称
    //@Field(type = FieldType.Text)
    private String spec;//规格
    //@Field(type = FieldType.Integer)
    private Integer saleNum;//销量
   // @Field(type = FieldType.Integer)
    private Integer commentNum;//评论数
    private Map<String, Object> specMap;
   // @Field(type = FieldType.Text)
    private String status;//商品状态 1-正常，2-下架，3-删除

    @Override
    public String toString() {
        return "SkuInfo{" +
                "id='" + id + '\'' +
                ", sn='" + sn + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", num=" + num +
                ", alertNum=" + alertNum +
                ", image='" + image + '\'' +
                ", images='" + images + '\'' +
                ", weight=" + weight +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", spuId='" + spuId + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", brandName='" + brandName + '\'' +
                ", spec='" + spec + '\'' +
                ", saleNum=" + saleNum +
                ", commentNum=" + commentNum +
                ", specMap=" + specMap +
                ", status='" + status + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getAlertNum() {
        return alertNum;
    }

    public void setAlertNum(Integer alertNum) {
        this.alertNum = alertNum;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSpuId() {
        return spuId;
    }

    public void setSpuId(String spuId) {
        this.spuId = spuId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public Map<String, Object> getSpecMap() {
        return specMap;
    }

    public void setSpecMap(Map<String, Object> specMap) {
        this.specMap = specMap;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SkuInfo(String id, String sn, String name, Long price, Integer num, Integer alertNum, String image, String images, Integer weight, Date createTime, Date updateTime, String spuId, Integer categoryId, String categoryName, String brandName, String spec, Integer saleNum, Integer commentNum, Map<String, Object> specMap, String status) {
        this.id = id;
        this.sn = sn;
        this.name = name;
        this.price = price;
        this.num = num;
        this.alertNum = alertNum;
        this.image = image;
        this.images = images;
        this.weight = weight;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.spuId = spuId;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.brandName = brandName;
        this.spec = spec;
        this.saleNum = saleNum;
        this.commentNum = commentNum;
        this.specMap = specMap;
        this.status = status;
    }

    public SkuInfo() {
    }
}
