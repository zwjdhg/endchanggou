package com.changgou.search.feign;

import com.changgou.util.Result;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @ClassName: SkuFeign
 * @Author: ZWJ
 * @Date: 2020-09-01 17:57
 * @Synopsis: Null
 **/
@FeignClient(name = "search")
@RequestMapping("/search")
public interface SkuFeign {
    /***
     * 调用搜索实现,返回查询数据
     */
    @GetMapping
    public Result<Map> search(@RequestParam(required = false)
                                          Map<String,String> searchMap) throws Exception;
}
