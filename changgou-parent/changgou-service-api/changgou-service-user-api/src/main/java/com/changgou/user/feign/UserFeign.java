package com.changgou.user.feign;

import com.changgou.user.pojo.User;
import com.changgou.util.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: UserFeign
 * @Author: ZWJ
 * @Date: 2020-09-05 20:17
 * @Synopsis: Null
 **/
@FeignClient( name = "user")
@RequestMapping("/user")
public interface UserFeign {
    /***
     * 根据ID查询User数据
     * @param id
     * @return
     */

    @GetMapping("/load/{username}")
    public Result<User> findById(@PathVariable(value = "username") String id);
    /***
     * 更新用户积分
     * @param points
     * @return
     */
    @GetMapping("/points/{points}")
    public Result addPoints(@PathVariable(value = "points") Integer points);
}
