package com.changgou.goods.Feign;

import com.changgou.goods.pojo.Sku;
import com.changgou.goods.pojo.Spu;
import com.changgou.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: skuFeign
 * @Author: ZWJ
 * @Date: 2020-08-29 16:06
 * @Synopsis: Null
 **/
@FeignClient(name = "service-goods")
@RequestMapping("/sku")
public interface skuFeign {
    /**
     * 根据状态查询SKU列表
     *
     * @param status
     */
    @GetMapping("/status/{status}")
    @ResponseBody
    public Result<List<Sku>> findByStatus(@PathVariable("status") String status);

    /**
     * 根据条件搜索
     * @param sku
     * @return
     */
    @PostMapping(value = "/search" )
    Result<List<Sku>> findList(@RequestBody(required = false) Sku sku);
    /***
     * 根据ID查询Sku数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable(value = "id") String id);

    /****
     * 库存递减实现
     */
    @GetMapping(value = "/decr/count")
    public Result decrCount(@RequestParam Map<String,String> map);
}
