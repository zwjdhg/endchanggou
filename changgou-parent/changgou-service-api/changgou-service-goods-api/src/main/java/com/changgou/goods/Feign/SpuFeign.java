package com.changgou.goods.Feign;

import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import com.changgou.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: SpuFeign
 * @Author: ZWJ
 * @Date: 2020-09-03 18:46
 * @Synopsis: Null
 **/
@FeignClient("service-goods")
@RequestMapping("/spu")
public interface SpuFeign {
    /***
     * 根据SPU的ID查找SPU以及对应的SKU集合
     * @param id
     * @return
     */
    @GetMapping("/goods/{id}")
    @ResponseBody
    public Result<Goods> findGoodsById(@PathVariable("id") String id);
    //查询全部spu
    @GetMapping
    public Result<List<Spu>> findAll();

    /***
     * 根据SpuID查询Spu信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Spu> findById(@PathVariable(name = "id") String id);

}
