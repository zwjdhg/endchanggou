package com.changgou.goods.Feign;

import com.changgou.goods.pojo.Category;
import com.changgou.util.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName: CategoryFeign
 * @Author: ZWJ
 * @Date: 2020-09-03 18:43
 * @Synopsis: Null
 **/
@FeignClient(name="service-goods")
@RequestMapping(value = "/category")
public interface CategoryFeign {
    /**
     * 获取分类的对象信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ResponseBody
    Result<Category> findById(@PathVariable(name = "id") Integer id);
}
