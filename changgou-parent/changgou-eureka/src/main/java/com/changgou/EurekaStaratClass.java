package com.changgou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @ClassName: EurekaStaratClass
 * @Author: ZWJ
 * @Date: 2020-08-23 16:37
 * @Synopsis: Null
 **/
@SpringBootApplication
@EnableEurekaServer
public class EurekaStaratClass {
    public static void main(String[] args) {
        SpringApplication.run(EurekaStaratClass.class,args);
    }
}
