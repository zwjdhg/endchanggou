package com.changgou.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.changgou.goods.Feign.CategoryFeign;
import com.changgou.goods.Feign.SpuFeign;
import com.changgou.goods.pojo.Category;
import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Sku;
import com.changgou.goods.pojo.Spu;
import com.changgou.service.ItemService;
import com.changgou.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @ClassName: ItemServiceImpl
 * @Author: ZWJ
 * @Date: 2020-09-03 17:01
 * @Synopsis: Null
 **/
@Service
public class ItemServiceImpl implements ItemService {
    //静态文件所存储的位置
 @Value("${pagepath}")
 private String pagePath;
 @Autowired
 private CategoryFeign categoryFeign;
 @Autowired
 private SpuFeign spuFeign;
 //模板引擎对象
    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 创建指定spu的静态页面
     *
     * @param spuId
     */
    @Override
    public void createHtml(String spuId){
        Result<List<Spu>> all = spuFeign.findAll();
        for (Spu datum : all.getData()) {
            //通过spuid查询商品的信息
            Result<Goods> goodsById = spuFeign.findGoodsById(datum.getId());
            Goods data = goodsById.getData();
            if (data!=null){
                //获取spu信息
                Spu spu = data.getSpu();
                //获取规格的json串
                String specItems = spu.getSpecItems();
                Map<String,String> specMap = JSONObject.parseObject(specItems, Map.class);
                //查看三级分类
                Category category1 = categoryFeign.findById(spu.getCategory1Id()).getData();
                Category category2 = categoryFeign.findById(spu.getCategory2Id()).getData();
                Category category3 = categoryFeign.findById(spu.getCategory3Id()).getData();

                //获取skulist的信息
                List<Sku> skuList = data.getSkuList();

                Map<String,Object> resultMap=new HashMap<>();
                resultMap.put("spu", spu);
                resultMap.put("category1", category1);
                resultMap.put("category2", category2);
                resultMap.put("category3", category3);
                resultMap.put("skuList", skuList);
                resultMap.put("specList", specMap);
                resultMap.put("imageList", spu.getImages().split(","));
                createPageHtml(resultMap,datum.getId());

            }

        }
    }
    /**
     * 根据SPUID生成静态页
     *
     * @param id : SpuId
     */
    @Override
    public void createPageHtml( Map<String,Object> resultMap,String id) {
        PrintWriter printWriter=null;
            //创建上下文对象
            Context context = new Context();
            //数据模型，用于存储页面需要填充的数据
            context.setVariables(resultMap);
        try {
            //生成的静态页面位置
            File file=new File(pagePath,id+".html");
            //生成静态页
            printWriter = new PrintWriter(file, "UTF-8");
            /*
            * 模板名字
            * 上下文对象
            * 文件输出对象
            */
            templateEngine.process("item",context,printWriter);

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            printWriter.close();
        }
    }
}
