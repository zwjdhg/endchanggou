package com.changgou.controller;

import com.changgou.service.ItemService;
import com.changgou.util.Result;
import com.changgou.util.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: ItemController
 * @Author: ZWJ
 * @Date: 2020-09-03 17:21
 * @Synopsis: Null
 **/
@RestController
@RequestMapping("/page")
@CrossOrigin
public class ItemController {
    @Autowired
    private ItemService itemService;
    /**
     * 根据SPUID生成静态页
     *
     * @param id : SpuId
     */
    @GetMapping(value = "/createHtml/{id}")
    //@ResponseBody
    public Result createPageHtml(@PathVariable(value = "id") String id) {
        itemService.createHtml(id);
        return new Result(true, StatusCode.OK, "生成成功！");
    }
}
