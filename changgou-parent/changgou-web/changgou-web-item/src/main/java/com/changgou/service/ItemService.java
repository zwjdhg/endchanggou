package com.changgou.service;

import java.util.Map;

/**
 * @ClassName: ItemService
 * @Author: ZWJ
 * @Date: 2020-09-03 17:01
 * @Synopsis: Null
 **/
public interface ItemService {
    /**
     * 根据SPUID生成静态页
     * @param id : SpuId
     */
    void createPageHtml(Map<String,Object> resultMap,String id);
    /**
     * 创建指定spu的静态页面
     *
     * @param spuId
     */
    void createHtml(String spuId);
}
