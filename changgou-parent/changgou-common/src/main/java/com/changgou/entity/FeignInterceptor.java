package com.changgou.entity;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @ClassName: FeignInterceptor
 * @Author: ZWJ
 * @Date: 2020-09-06 17:42
 * @Synopsis: Null
 * 进行feign调用时配置
 **/
public class FeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
            //获取request相关变量
            ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (requestAttributes!=null){
                //取出request
                HttpServletRequest request = requestAttributes.getRequest();
                //获取所有头文件的key
                Enumeration<String> headerNames = request.getHeaderNames();
                if (headerNames!=null){
                    //遍历所有key
                    while (headerNames.hasMoreElements()){
                        //取出key
                        String key = headerNames.nextElement();
                        //取出相应的头信息
                        String header = request.getHeader(key);
                        //将头信息添加到头文件中
                        requestTemplate.header(key,header);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
