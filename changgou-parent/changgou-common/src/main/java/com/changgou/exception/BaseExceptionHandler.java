package com.changgou.exception;

import com.changgou.util.Result;
import com.changgou.util.StatusCode;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName: BaseExceptionHandler
 * @Author: ZWJ
 * @Date: 2020-08-23 21:56
 * @Synopsis: Null
 **/
@ControllerAdvice
public class BaseExceptionHandler {
   @ExceptionHandler(value = Exception.class)
   @ResponseBody
    public Result Exception(Exception e)throws TransactionException {
        e.printStackTrace();
        int a=10;
        int v=10;
        int b=10;
        int c=10;
        return new Result(false, StatusCode.ERROR,"未知错误,正在维护",e.getMessage());
    }
}
