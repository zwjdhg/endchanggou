package com.changgou.oauth.config;

import com.changgou.oauth.util.JwtToken;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.condition.RequestConditionHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @ClassName: FeignOauth2RequestInterceptor
 * @Author: ZWJ
 * @Date: 2020-09-06 17:28
 * @Synopsis: Null
 **/
@Configuration
public class FeignOauth2RequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
            //创建令牌
            String token= "Bearer "+ JwtToken.adminJwt();
            //将令牌添加到头文件
            requestTemplate.header("Authorization",token);
           /* //获取request相关变量
            ServletRequestAttributes requestAttributes =(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (requestAttributes!=null){
                //取出request
                HttpServletRequest request = requestAttributes.getRequest();
                //获取所有头文件的key
                Enumeration<String> headerNames = request.getHeaderNames();
                if (headerNames!=null){
                    //遍历所有key
                    while (headerNames.hasMoreElements()){
                        //取出key
                        String key = headerNames.nextElement();
                        //取出相应的头信息
                        String header = request.getHeader(key);
                        //将头信息添加到头文件中
                        requestTemplate.header(key,header);
                    }
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
