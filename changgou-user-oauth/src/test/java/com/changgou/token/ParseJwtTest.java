package com.changgou.token;

import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

/*****
 * @Author: shenkunlin
 * @Date: 2019/7/7 13:48
 * @Description: com.changgou.token
 *  使用公钥解密令牌数据
 ****/
public class ParseJwtTest {

    /***
     * 校验令牌
     */
    @Test
    public void testParseToken(){
        //令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6IlJPTEVfVklQLFJPTEVfVVNFUiIsIm5hbWUiOiJpdGhlaW1hIiwiaWQiOiIxIn0.cP4BFEN7Mt849ahT_qYzeX_GkqmMu_EXz92QGtBCDXQZcYyF0E3KrumUU6q4Gi7nvJ1osBzHbV-NqY50VKAkG2_o2NkjI3TN4FEFTmEvF63xmW_vDH2KXWE7nxUW30_A-M_wmzbXW2e9Cs0r5Qndm-_56H_BGjoLBmj5kjitgsdm95boG0Jo0ndvW6N9i73be-rLNWxvaq4KDpAoysgrqWnNMNBLzfMIGP876Crewlu7gsMQCexDhe9om0WSH6YPY6aW-qwXzTcllmSW7v6NbomSIBmmO9Tj_AnmWdNYGogjsiq-yScNP_HnnMd4KuAFwPY1fGL1zwReYuHecYSnuQ";

        //公钥
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwvSyW36TmCgSGeiS6ESthEgoZiPcMj14KG7zi1CJGfApCFO0NX2RsnjhmurkpRrVPFcLi7QzJYwcek5V5olKVWVYwpStsS55QYfJL1vGGBoDyNON95VdQm/nE1q/s65h2dChC3hbGTsFAHBv+ItThQ4ONBhdPaPK2Is4ZXlTUTJVi1ur652DlbmsngYYjTbuLBem558P8qZms4Dp66JMp8PWjIzQixhkLvZGyrhF55WdSwxLZm2qbPxVxy1xOxEHb+lxLGL6kcUoACQAS+PIjcLa4ISIJmR6xkrrvYcgQJoALMmy0+RD91bPAi629xMdLhPwSPOraw0r+zzbHB38TwIDAQAB-----END PUBLIC KEY-----";

        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));

        //获取Jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }
}
